import React from "react";
import './Button.css';
import {buttonTypes} from "./types";


const Button = ({ text, type, handleClick}) => {
    return (
        <button onClick={handleClick} className={`btn ${buttonTypes[type]}`}>
            {text}
        </button>
    )
};

export default Button