import React from "react";
import Button from "../Button";
import "./Modal.css"
import {useDispatch, useSelector} from "react-redux";
import {closeCartModalAction} from "../../../store/actions";
const Modal = ({title, children}) =>{
    const modalCartOpen = useSelector(state => state.modalCartOpen);
    const display = modalCartOpen ? 'show' : ''
    const dispatch = useDispatch();
    const closeCartModal = ()=> dispatch (closeCartModalAction());
    return(
            <div className={`modal-dialog modal-lg ${display}`} role="document">
                <div className="modal-content">
                    <div className="modal-header">
                        <h5 className="modal-title">{title}</h5>
                        <Button type="button" text='&times;' handleClick={closeCartModal}/>
                    </div>
                    <div className="modal-body">
                        {children}
                    </div>
                    <div className="modal-footer">
                        <Button type="button" text='Close' handleClick={closeCartModal}/>
                    </div>
                </div>
            </div>
    )
}
export default Modal