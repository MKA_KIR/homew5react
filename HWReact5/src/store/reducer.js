import {OPEN_CART_MODAL, CLOSE_CART_MODAL, CLEAR_CART, ADD_TO_CART, DEL_FROM_CART, INCREASE_FROM_CART, DECREASE_FROM_CART} from './constants'
import {initialState} from "./initialState";

export const reducer = (state = initialState, {type, payload}) =>{
    const copyCart = state.cart.map(item => ({...item}))
    switch (type){
        case OPEN_CART_MODAL:
            return {...state, modalCartOpen: true}

        case   CLOSE_CART_MODAL:
            return {...state, modalCartOpen: false}
        case ADD_TO_CART:
            const product = copyCart.find( ({id}) => id === payload.id)
            if (!product) {
                const newProduct = {...payload, count: 1}
                const  newCart = [...copyCart, newProduct];
                localStorage.setItem('cart', JSON.stringify(newCart))
                return {...state, cart: newCart};
            } else {
                product.count++
                localStorage.setItem('cart', JSON.stringify(copyCart))
                return {...state, cart:copyCart}
            }

        case CLEAR_CART:
            localStorage.setItem('cart', JSON.stringify([]))
            return {...state, cart:[]}

        case DEL_FROM_CART:
            copyCart.splice(payload, 1)
            localStorage.setItem('cart', JSON.stringify(copyCart))
            return {...state, cart:copyCart}

        case INCREASE_FROM_CART:
            copyCart[payload].count++
            localStorage.setItem('cart', JSON.stringify(copyCart))
            return {...state, cart:copyCart}

        case DECREASE_FROM_CART:
            copyCart[payload].count--
            if( !copyCart[payload].count ){
                copyCart.splice(payload, 1)
            }
            localStorage.setItem('cart', JSON.stringify(copyCart))
            return {...state, cart:copyCart}
        default:
            return state
    }
}