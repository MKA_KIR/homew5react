import {DECREASE_FROM_CART} from "../constants";

export const decreaseFromCartAction = (idx)=> {
    return {
        type: DECREASE_FROM_CART,
        payload: idx
    }
}