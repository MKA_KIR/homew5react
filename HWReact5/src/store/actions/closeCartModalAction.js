import {CLOSE_CART_MODAL} from "../constants";

export const closeCartModalAction = ()=> {
    return {
        type: CLOSE_CART_MODAL
    }
}