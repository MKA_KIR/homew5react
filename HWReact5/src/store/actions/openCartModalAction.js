import {OPEN_CART_MODAL} from "../constants";

export const openCartModalAction = ()=> {
    return {
        type: OPEN_CART_MODAL
    }
}