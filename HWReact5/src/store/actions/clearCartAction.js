import {CLEAR_CART} from "../constants";

export const clearCartAction = ()=> {
    return {
        type: CLEAR_CART
    }
}