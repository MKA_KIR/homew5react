import {INCREASE_FROM_CART} from "../constants";

export const increaseFromCartAction = (idx)=> {
    return {
        type: INCREASE_FROM_CART,
        payload: idx
    }
}
