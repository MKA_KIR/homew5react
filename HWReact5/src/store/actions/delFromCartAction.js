import {DEL_FROM_CART} from "../constants";

export const delFromCartAction = (idx)=> {
    return {
        type: DEL_FROM_CART,
        payload: idx
    }
}