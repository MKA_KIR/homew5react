import React from "react";
import ProductCard from "../../../Products/components/ProductCard";
import {useSelector} from "react-redux";


const ProductsListPage = () => {
    const productList =useSelector(state => state.productList)
    const productsElements = productList.map(item => <ProductCard {...item} /> );
    return (
        <div className='container'>
            <div className='row'>
                {productsElements}
        </div>
        </div>
    )
}

export default ProductsListPage;