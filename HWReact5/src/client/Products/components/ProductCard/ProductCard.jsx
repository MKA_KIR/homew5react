import React from "react";
import Button from "../../../../Shared/Components/Button";
import './ProductCard.css'
import {useDispatch} from "react-redux";
import {addToCartAction} from "../../../../store/actions";
const ProductCard = (product) =>{
    const {src, title, price} = product;
    const dispatch = useDispatch();
    const addToCart = ()=> dispatch(addToCartAction(product));
    return(
        <div className="col">
            <div className="card product-card">
                <img className="card-img-top" src={src} alt="Card image cap"/>
                    <div className="card-block">
                        <h4 className="card-title">{title}</h4>
                        <p className="card-text">Price: ${price}</p>
                        <Button type='primary' text='Add To Cart' handleClick={()=>addToCart(product)}/>
                    </div>
            </div>
        </div>
    )
}
export default ProductCard