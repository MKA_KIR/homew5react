import React from "react";

const CartItem = (props) =>{
    const {title, price, count, decreaseItem, increaseItem, deleteItem} = props
    const totalPrice = price*count
    return(
        <tr>
            <td>{title}</td>
            <td>({price})</td>
            <td>
                <div className="input-group">
                    <button className="minus-item input-group-addon btn btn-primary" data-name="Orange" onClick={decreaseItem}>-</button>
                    <input type="number" className="item-count form-control" data-name="Orange" value={count} />
                    <button className="plus-item btn btn-primary input-group-addon" data-name="Orange" onClick={increaseItem}>+</button>
                </div>
            </td>
            <td>
                <button className="delete-item btn btn-danger" data-name="Orange" onClick={deleteItem}>X</button>
            </td>
            <td>{totalPrice}</td>
        </tr>
    )
};

export default CartItem