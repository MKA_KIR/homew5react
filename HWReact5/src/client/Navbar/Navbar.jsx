import React from "react";
import './Navbar.css';
import Button from "../../Shared/Components/Button";
import {componentsProps} from "./componentsProps";
import {useDispatch, useSelector} from "react-redux";
import {openCartModalAction, clearCartAction} from "../../store/actions";

const Navbar = () =>{
    const cart = useSelector(state => state.cart);
    const dispatch = useDispatch();
    const openCartModal = ()=> dispatch (openCartModalAction());
    const clearCart = ()=> dispatch (clearCartAction());
    const {openCartButton, clearCartButton} = componentsProps;
    let totalCartProducts = 0;
    cart.forEach(({count})=>totalCartProducts+=count);
    return(
        <div className="navbar">
            <div className="container">
                <div className="navbar-row">
                    <Button {...openCartButton}  text={`Cart (${totalCartProducts})`} handleClick={openCartModal} />
                    <Button {...clearCartButton} handleClick={clearCart}/>
                </div>
            </div>
        </div>
    )
}
export default Navbar