export const componentsProps = {
    openCartButton: {
        text: "Cart",
        type: "primary",
    },
    clearCartButton: {
        text: "Clear Cart",
        type: "danger"
    }
}